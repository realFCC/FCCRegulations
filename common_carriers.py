import lib_congress

common_carriers = lib_congress.CommunicationActOf1934.GetCommonCarriers()

def carrier_is_neutral(carrier):
    return carrier in common_carriers
